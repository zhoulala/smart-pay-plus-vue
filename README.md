## 开发
 smart-pay-plus前台界面，基于若依修改。后端见：https://gitee.com/zhunian/smart-pay-plus

```bash
# 克隆项目
git clone https://gitee.com/zhunian/smart-pay-plus-vue.git

# 进入项目目录
cd smart-pay-plus-vue

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 http://localhost:8088

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```